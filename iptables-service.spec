#
# spec file for package iptables-service
#
# Based on the scripts from http://pkgs.fedoraproject.org/cgit/iptables.git
#
# 
#
# Please reports bugs to https://chaos.expert/wonko/iptables-service/issues
#

Name: iptables-service
Summary: Service for persistent iptables rules
Version: 0.1
Release: 0.1
Source:  %{name}-%{version}.tar.bz2
URL: https://chaos.expert/wonko/iptables-service 
License: GPLv2
Requires: iptables
%{?systemd_requires}

%description
iptables rules are not persitent across reboots. This systemd service
loads iptables rules previously stored.


%prep
%setup -q

%build

%install
%__install -D iptables.service %{buildroot}/%{_unitdir}/
%__install -D ip6tables.service %{buildroot}/%{_unitdir}/

%pre
%service_add_pre iptables.service ip6tables.service

%post
%service_add_post iptables.service ip6tables.service

%preun
%service_del_preun iptables.service ip6tables.service

%postun
%service_del_postun iptables.service ip6tables.service


%files 
%config(noreplace) %{_sysconfdir}/sysconfig/iptables
%config(noreplace) %{_sysconfdir}/sysconfig/ip6tables
%{_unitdir}/iptables.service
%{_unitdir}/ip6tables.service


